<!doctype html>
<html lang="en">
  <head>
    <?php include_once('src/header.php') ?>
    <title>Prueba Konecta</title>
  </head>
  <body>
    <div class="container-fluid">
        <div class="container">
          <h1>Inventario de productos</h1>
          <button type="button" class="btn btn-primary" id="crear" style= "margin-bottom: 20px;">Agregar Producto</button>
            <div class="row"> 
                <table class="table" id="tblProductos">
                    <thead>
                        <tr>
                        <th>Nombre de producto</th>
                        <th scope="col">Referencia</th>
                        <th scope="col">Precio</th>
                        <th scope="col">Peso</th>
                        <th scope="col">Categoría</th>
                        <th scope="col">Stock</th>
                        <th scope="col">Fecha de creación</th>
                        <th scope="col">Fecha de  ultima venta</th>
                        <th scope="col">Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                       
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Inicio modal -->
    <?php include_once('modal/modal.php')?>
    <!-- fin modal -->
    <?php include_once('src/footer.php')?>
    <script src="js/productos.js" ></script>
  </body>
</html>