# CONFIGURACION DEL PROYECTO
1) Crear una base de datos con le nombre prueba_konecta, o el de su preferencia
  *** CREATE DATABASE prueba_konecta ***

2) Importar el archivo prueba_konecta.sql, Ubicado en la carpeta sql del proyecto.

3) Clonamos el proyecto (git clone https://gitlab.com/tajercalvo/prueba-konecta)

3) Modificamos el arcvhivo datos.php, ubicado en la carpeta config , ingresando el usuario, contraseña y el nombre de la base de datos que se creó.
    $datos = [
            "host"     => "localhost",
            "user"     => "root",
            "pass"     => "",
            "db"       => "prueba_konecta",
            "path"     => "",
            "key"      => ''
        ];

# CARPETAS DEL PROYECTO
siguiendo el paradigma MVC
1) *** Carpeta config ***
   Carpeta en la cual encontramos la configuracion de la conexion a la DB
2) *** Carpeta View ***
   carpeta en la cual encontramos la vista de nustro proyecto (index.php)
   2.1) *** Carpeta Modal ***
        ubicado un archivo modal.php , el cual contiene todas las modales utilizada en nustro proyecto
   2.2) *** Carpeta src ***
         Ubicado archivo header.php y footer.php
   2.3) *** carpeta js ***
         Ubicado archivo productos.js , el cual nospermite la interactivida de todo el proyecto 
2) *** Carpeta  Controllers ***
    Ubicado el archivo producto.php , el cual nos controla la interactividad que existe entre la vista y el modelo
3) *** Carpeta  Models ***
   ubicado el archivo productos.php , el cual nos permite la comunicacion con nuestra DB. 
