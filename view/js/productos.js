// Copyright 1999-2021. Plesk International GmbH. All rights reserved.
$(document).ready(function(){
    listarProductos()
})
/**
 *  @name Listar_Productos 
 * @Descripcion:  Metodo para listar los productos 
*/
function listarProductos(){
    $.ajax({
        url: '../controllers/productos.php',
        type: 'POST',
        dataType: 'json',
        data: {opcn:'listarProductos'},
    })
    .done(function(res) {
        let tr = res.map((el,index) => {
            let fecha_ultima_venta = (el.fecha_ultima_venta == null) ? '' : el.fecha_ultima_venta
           return `<tr>
                    <td scope="row">${el.nombre_producto}</td>
                    <td>${el.referencia}</td>
                    <td>${el.precio}</td>
                    <td>${el.peso}</td>
                    <td>${el.categoria}</td>
                    <td>${el.stock}</td>
                    <td>${el.fecha_creacion}</td>
                    <td>${fecha_ultima_venta}</td>
                    <td>
                       <button type="button" class="btn btn-primary btn-sm editar" data-id= "${el.id_producto}">Editar</button>
                       <button type="button" class="btn btn-danger btn-sm eliminar" data-id= "${el.id_producto}">Eliminar</button>
                       <button type="button" class="btn btn-success btn-sm vender" data-id= "${el.id_producto}" data-stock= "${el.stock}">Vender</button>
                    </td>
                    </tr>
                    <tr>`
       })
       $('#tblProductos tbody').html(tr);
    })
    .fail(function() {
        console.log("error");
    })
}
/**
 * @Descripcion:  Evento que me permite mostrar la modal de Crear y resetear el formulario 
*/
$('body').on('click','#crear',function() {
    console.log('crear')
    $('#modCrear').modal('show')
    document.getElementById("fromCrear").reset();
})
/** 
 * * * @name Crear_Producto
 * @Descripcion:  Evento submit,Evento que me permite Crear un nuevo producto  
*/
$('#modCrear').submit(function(e){
    e.preventDefault();
    //guardo los datos del formularion en un array de objeto
    let datos = $('#fromCrear').serializeArray();
    datos.push({name:'opcn',value:'crear'})
    
    $.ajax({
        url: '../controllers/productos.php',
        type: 'POST',
        dataType: 'json',
        data: datos,
    })
    .done(function(res) {
        $('#modCrear').modal('hide');
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Operacion Exitosa',
            showConfirmButton: false,
            timer: 1500
          })
       listarProductos()
    })
    .fail(function() {
        console.log("error");
    })  
})
/** 
 * @name Ver_Productos_X_Id
 * @Descripcion:  Evento Click Editar,Evento que me permite Ver la informacion de un producto por medio del id  
*/
$('body').on('click','.editar', function(){
    let id = $(this).data('id')
    $.ajax({
        url: '../controllers/productos.php',
        type: 'POST',
        dataType: 'json',
        data: {opcn: 'verXid',id},
    })
    .done(function(res) {
        $('#modEditar').modal('show');
        $('#id').val(res.id_producto)
        $('#nombre1').val(res.nombre_producto)
        $('#referencia1').val(res.referencia)
        $('#precio1').val(res.precio)
        $('#peso1').val(res.peso)
        $('#categoria1').val(res.categoria)
        $('#stock1').val(res.stock)
    })
    .fail(function() {
        console.log("error");
    })
})
/** 
 * @name Actualizar
 * @Descripcion:  Evento submit Actualizar, Evnto que me permite actualizar la informacion en DB
*/
$('#modEditar').submit(function(e){
    e.preventDefault();
    let datos = $('#fromEditar').serializeArray();
    datos.push({name:'opcn',value:'update'})
    
    $.ajax({
        url: '../controllers/productos.php',
        type: 'POST',
        dataType: 'json',
        data: datos,
    })
    .done(function(res) {
        $('#modEditar').modal('hide');
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Operacion Exitosa',
            showConfirmButton: false,
            timer: 1500
          })
       listarProductos()
    })
    .fail(function() {
        console.log("error");
    })  
})
/** 
 * @name Eliminar
 * @Descripcion:  Evento Eliminar, Evento que me permite Eliminar un producto en la Db por id
*/
$('body').on('click','.eliminar', function(){
    let id = $(this).data('id')
    Swal.fire({
        title: 'Esta seguro de eliminar este producto?',
        text: "¡No podrás revertir esto!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si'
      }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '../controllers/productos.php',
                type: 'POST',
                dataType: 'json',
                data: {opcn: 'eliminarXid',id},
            })
            .done(function(res) {
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Operacion Exitosa',
                    showConfirmButton: false,
                    timer: 1500
                  })
               listarProductos()
            })
            .fail(function() {
                console.log("error");
            }) 
          
        }
      })
})
/** 
 * @name Vender
 * @Descripcion:  Evento Evender, Evento que me permite Vender productos por id
*/
$('body').on('click','.vender', function(){
    let id = $(this).data('id')
    let stock = $(this).data('stock')
    if(stock <= 0){
        Swal.fire(
            '¡Oops¡',
            'No cuenta con productos en el Stock',
            'info'
          )
          return;
    }
    Swal.fire({
        title: 'Esta seguro de Vender este producto?',
        text: "¡No podrás revertir esto!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si'
      }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: '../controllers/productos.php',
                type: 'POST',
                dataType: 'json',
                data: {opcn: 'ventaXid',id},
            })
            .done(function(res) {
                console.log(res)
                Swal.fire(
                    `${res.title}`,
                    `${res.mensaje}`,
                    `${res.icono}`
                  )
               listarProductos()
            })
            .fail(function() {
                console.log("error");
            }) 
          
        }
      })
})