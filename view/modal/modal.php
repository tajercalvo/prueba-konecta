
<!-- INICIO MODAL CREAR -->
<div class="modal fade" id="modCrear" tabindex="-1" role="dialog" aria-labelledby="modCrear" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Crear Producto</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form id="fromCrear">
            <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nombre Producto</label>
                        <input type="text" class="form-control" id="nombre" name="nombre" aria-describedby="Nombre" placeholder="Nombre Producto" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Referencia</label>
                        <input type="text" class="form-control" id="referencia" name="referencia" aria-describedby="Referencia" placeholder="Referencia" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Precio</label>
                        <input type="number" class="form-control" id="precio" name="precio" aria-describedby="Precio" placeholder="Precio" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Peso</label>
                        <input type="number" class="form-control" id="peso" name="peso" aria-describedby="Peso" placeholder="Peso" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Categoria</label>
                        <input type="text" class="form-control" id="categoria" name="categoria" aria-describedby="categoria" placeholder="Categoria" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Stock</label>
                        <input type="number" class="form-control" id="stock" name="stock" aria-describedby="Stock" placeholder="Stock" required>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary" id="insertGuradar">Guardar</button>
            </div>
        </form>
        </div>
    </div>
</div>
<!-- FIN MODAL CREAR -->
<!-- INICIO MODAL Editar -->
<div class="modal fade" id="modEditar" tabindex="-1" role="dialog" aria-labelledby="modCrear" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Editar Producto</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form id="fromEditar">
            <div class="modal-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nombre Producto</label>
                        <input type="text" class="form-control" id="nombre1" name="nombre1" aria-describedby="Nombre" placeholder="Nombre Producto" required>
                        <input type="hidden" class="form-control" id="id" name="id">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Referencia</label>
                        <input type="text" class="form-control" id="referencia1" name="referencia1" aria-describedby="Referencia" placeholder="Referencia" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Precio</label>
                        <input type="number" class="form-control" id="precio1" name="precio1" aria-describedby="Precio" placeholder="Precio" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Peso</label>
                        <input type="number" class="form-control" id="peso1" name="peso1" aria-describedby="Peso" placeholder="Peso" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Categoria</label>
                        <input type="text" class="form-control" id="categoria1" name="categoria1" aria-describedby="categoria" placeholder="Categoria" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Stock</label>
                        <input type="number" class="form-control" id="stock1" name="stock1" aria-describedby="Stock" placeholder="Stock" required>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-primary" id="insertGuradar">Actualizar</button>
            </div>
        </form>
        </div>
    </div>
</div>
<!-- FIN MODAL Editar -->