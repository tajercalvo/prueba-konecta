<?php  
    $method = $_SERVER['REQUEST_METHOD'];
    switch ($method) {
        case 'POST':
            include_once('../models/productos.php');
            switch ($_POST['opcn']){
                case 'listarProductos':
                    $respuesta = Productos::listarProductos();
                    echo json_encode($respuesta);
                    break;
                case 'crear':
                    $respuesta = Productos::crear($_POST);
                    echo json_encode($respuesta);
                    break;
                case 'verXid':
                    $respuesta = Productos::verXid($_POST);
                    echo json_encode($respuesta);
                    break;
                case 'update':
                    $respuesta = Productos::update($_POST);
                    echo json_encode($respuesta);
                    break;
                case 'eliminarXid':
                    $respuesta = Productos::eliminarXid($_POST);
                    echo json_encode($respuesta);
                    break;
                case 'ventaXid':
                    $respuesta = Productos::ventaXid($_POST);
                    echo json_encode($respuesta);
                    break;
        }
            break; 
    }
     