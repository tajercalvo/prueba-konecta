<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- CDN Bootstrap CSS -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<!-- Estilos personalizados -->
<style>
    h1 {
      text-align: center;
      text-transform: uppercase;
      padding-top: 30px;
      padding-bottom: 20px;
    }
    .form-group {
      margin-bottom: 1rem;
    }
    button.close {
    padding: 0;
    background-color: transparent;
    border: 0;
    -webkit-appearance: none;
    }
    .close {
      float: right;
      font-size: 1.5rem;
      font-weight: 700;
      line-height: 1;
      color: #000;
      text-shadow: 0 1px 0 #fff;
      opacity: .5;
    }
</style>