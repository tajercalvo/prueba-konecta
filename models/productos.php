<?php
class Productos {
    /**
    * @Descripcion:  Metodo que me permite listar todos los productos
    */
    public static function listarProductos(){
        include_once('../config/init_db.php');
        DB::$encoding = 'utf8';
        $resultado = DB::query("SELECT * FROM productos");
        return $resultado; 
    }
    /**
    * @Descripcion:  Metodo que me permite Crear productos
    */
    public static function crear($v){
        include_once('../config/init_db.php');
        DB::$encoding = 'utf8';
       $insert = DB::query("INSERT INTO productos(
                                        nombre_producto,
                                        referencia,
                                        precio,
                                        peso,
                                        categoria,
                                        stock,
                                        estado
                                    )
                                    VALUES(
                                        '{$v['nombre']}',
                                        '{$v['referencia']}',
                                        '{$v['precio']}',
                                        '{$v['peso']}',
                                        '{$v['categoria']}',
                                        '{$v['stock']}',
                                        '1'
                                    )");
        return $insert;
    }
     /**
    * @Descripcion:  Metodo que me permite Ver productos por id
    */
    public static function verXid($v){
        include_once('../config/init_db.php');
        DB::$encoding = 'utf8';
        $resultado = DB::queryFirstRow("SELECT * FROM `productos` WHERE id_producto = '{$v['id']}'");
        return $resultado;
    }
     /**
    * @Descripcion:  Metodo que me Actualizar productos por id
    */
    public static function update($v){
        include_once('../config/init_db.php');
        DB::$encoding = 'utf8';
       $update = DB::query("UPDATE
                                productos
                            SET
                                nombre_producto        = '{$v['nombre1']}',
                                referencia             = '{$v['referencia1']}',
                                precio                 = '{$v['precio1']}',
                                peso                   = '{$v['peso1']}',
                                categoria              = '{$v['categoria1']}',
                                stock                  = '{$v['stock1']}',
                                fecha_creacion         = NOW()
                            WHERE  id_producto         = '{$v['id']}'");
        return $update;
    }
     /**
    * @Descripcion:  Metodo que me permite Eliminar produstos por id
    */
    public static function eliminarXid($v){
        include_once('../config/init_db.php');
        DB::$encoding = 'utf8';
        $eliminar = DB ::query("DELETE FROM productos WHERE id_producto = '{$v['id']}'");
        return $eliminar; 
    }
     /**
    * @Descripcion:  Metodo que me permite Realizar ventas por id
    */
    public static function ventaXid($v){
        include_once('../config/init_db.php');
        DB::$encoding = 'utf8';
        $stock = DB::queryFirstRow("SELECT stock FROM productos WHERE id_producto = '{$v['id']}'");
        if($stock['stock'] > 0){
            $newStock = $stock['stock'] - 1;
            DB::query("UPDATE
                            productos
                        SET
                            stock = '{$newStock}',
                            fecha_ultima_venta = NOW()
                        WHERE
                            id_producto = '{$v['id']}'");
            $resultado["error"] = false;
            $resultado["title"] = 'Bien';
            $resultado["icono"] = 'success';
            $resultado["mensaje"] = 'Producto vendido Exitosamente';
        }else{
             $resultado["error"] = 'false';
             $resultado["title"] = '¡Oops¡';
             $resultado["icono"] = 'info';
             $resultado["mensaje"] = 'No cuenta con productos en el Stock';
        }
        return $resultado;
    }
}